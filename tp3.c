#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tp3.h"

void initialize(List** list){
    *list = malloc(sizeof(List));
    (*list)->head = NULL;
    (*list)->tail = NULL;
}

void insert_empty_list(List** list, char* str){
    if ((*list)->head || (*list)->tail){
        printf("List is not empty... use another insert function.\n");
        return;
    }

    // Tant qu'il reste quelque chose dans le str
    // Cr�er un �l�ment
    // Ins�rer les 5 premi�res lettres du str
    // Brancher l'�l�ment
    // Fin tant que
    // Cr�er un �l�ment vide
    // Le brancher

    int length = strlen(str), total = 0, i;

    while(length > 0){ // O(strlen(str)) car le second while est lié
        i = 0;
        Element* e = malloc(sizeof(Element));
        e->data = malloc(sizeof(char)*(N+1));
        while(length > 0 && i < 5){ // tant qu'il reste de l'input et que l'on a pas rempli la data
            *(e->data+i) = *(str+total+i);
            i++;
            length--;
        }
        *(e->data+i) = '\0';
        total += i;
        e->next = NULL;
        if (!(*list)->head)
            (*list)->head = e;
        else
            (*list)->tail->next = e;
        (*list)->tail = e;
    }

    Element* e_vide = malloc(sizeof(Element));
    e_vide->data = NULL;
    e_vide->next = NULL;
    (*list)->tail->next = e_vide;
    (*list)->tail = e_vide;//tail doit pointer sur le dernier element
}

void insert_beginning_list(List** list, char* str){
    // empty list
    if ((*list)->head == NULL && (*list)->tail == NULL){
        if (DEBUG) printf("The list is empty...\n");
        insert_empty_list(list, str);
        return;
    }

    List *tempList;
    initialize(&tempList);
    insert_empty_list(&tempList, str);
    tempList->tail->next = (*list)->head;
    (*list)->head = tempList->head;
    free(tempList);

}

void insert_end_list(List** list, char* str){
    if ((*list)->head == NULL && (*list)->tail == NULL){
        if (DEBUG) printf("The list is empty...\n");
        insert_empty_list(list, str);
        return;
    }

    List *tempList;
    initialize(&tempList);
    insert_empty_list(&tempList, str);
    (*list)->tail->next = tempList->head;
    (*list)->tail = tempList->tail;
    free(tempList);

}

int insert_after_position(List** list, char* str, int p){
    if (!(*list)){
        printf("List doesn't exist.\n");
        return -1;
    }
    int length;
    length = lengthList(*list);
    if((p <= 0) || (p > length)){
        printf("The provided value for the position is incorrect. If list is empty, use \"insert_empty_list\" function instead.\n");
        return -1;
    }

    if (p == 1 || length == 0){
        insert_beginning_list(list, str);
        return 0;
    }

    if (p == length || ((((*list)->head != NULL) & ((*list)->head == (*list)->tail)))){
        insert_end_list(list, str);
        return 0;
    }

    Element* parcour = (*list)->head;
    int compteur = 0;

    while(p != compteur){
        parcour = parcour->next;
        if(parcour->data == NULL)
            compteur++;
    }
    //a la sortie parcour est à l'element de delimitation de l'element p
    List *tempList;
    initialize(&tempList);
    insert_empty_list(&tempList, str);
    tempList->tail->next = parcour->next;
    parcour->next = tempList->head;
    free(tempList);
    return 0;
}

int remove_element(List* list, int p){
    if (!list){
        printf("List pointer is null.\n");
        return -1;
    }
    int length;
    length = lengthList(list);

    if(p <= 0 || p > length){
        printf("The provided value for the position is incorrect.\n");
        return -1;
    }

    int n = 1; // position du parcours
    Element* e = list->head;
    Element* suppr = list->head;

    /*
        e sera utilisé pour pointer la bonne position, suppr sera utilisé pour "manger le nombre" (désallocation)
    */

    while(n < p){ // tant que l'on est pas arrivé au Pème chiffre
        do{
            e = e->next;
        }while(e->data != NULL); // on parcoure la liste jusqu'au prochain nombre
        n++;
    }
    suppr = e->next;

    /* à ce niveau, on en est là :
                                P
    | ##### | ##### | ¤¤¤¤¤ | ##### | ##### | ¤¤¤¤¤ |
                        ^       ^
                        e     suppr
    */

    if (p == 1){ // c'est le premier élément de la liste qu'il faut supprimer
        e = malloc(sizeof(Element));
        e->data = NULL;
        e->next = suppr;
        list->head = suppr->next;
    }

    do{
        e->next = suppr->next;
        free(suppr->data);
        free(suppr);
        suppr = e->next;
    }while(suppr->data != NULL);

    if (suppr == list->tail){ // si c'était le dernier nombre de la liste
        free(list->tail->data);
        list->tail = e;
        e->next = NULL;
        free(suppr);
    }
    else{
        if (p == 1){
            list->head = suppr->next; // sinon on a supprimé au début de la liste
            free(e);
        }
        else
            e->next = suppr->next; // sinon on a supprimé au milieu de la liste
        free(suppr->data);
        free(suppr);
    }

    return 0;
}

int compare(char* str1, char* str2){
    if (strlen(str1) > strlen(str2)){
        return 1;
    }
    if (strlen(str1) < strlen(str2)){
        return 2;
    }
    if (*str1 == '\0' && *str2 == '\0'){
        return 2;
    }
    if (*str1 < *str2){
        return 2;
    }
    if (*str1 > *str2){
        return 1;
    }
    return compare(str1+1, str2+1);
}

int sort(List* list){ // tri à bulles
    if (DEBUG) printf("Sorting in progress...\n");
    int l = lengthList(list);

    if (!list){
        printf("List does not exist and can't be sorted.");
        return -1;
    }

    if (l <= 1){
        printf("Nothing to sort\n");
        return 0; // rien à trier
    }

    char* n1 = malloc(sizeof(char)*128); n1[0] = '\0';
    char* n2 = malloc(sizeof(char)*128); n2[0] = '\0';
    Element* start, *interval, *fh, *ft, *sh, *st, *read, *ox; // start, firsthead, firsttail, secondhead, secondtail
    // pour faire le tri à bulles, il faut repérer les deux nombres par leurs débuts et leurs fins, d'oû 4 pointeurs d'élement fh ft sh st
    // cela nous servira pour les facilement switcher les nombres

    interval = list->tail;
    start = list->head;
    fh = start;
    ft = fh;
    while(ft->next->data != NULL){
        ft = ft->next;
    }


    while (interval != list->head){
        do // un passage de bulle
        {
            sh = ft->next->next;
            st = sh;
            while(st->next->data != NULL){
                st = st->next;
            }
            for (read = fh; read != ft->next; read = read->next){ // on lit la première valeur
                n1 = strcat(n1, read->data); // concatenation
            }
            for (read = sh; read != st->next; read = read->next){ // on lit la première valeur
                n2 = strcat(n2, read->data); // concatenation
            }
            if (DEBUG) printf("Comparing %s and %s...\n", n1, n2);
            if (compare(n1, n2) == 1){
                ox = st->next;
                st->next = ft->next;
                ft->next = ox;
                st->next->next = fh;
               /* free(ft->next); // on libère l'élément de séparation des deux nombres
                ft->next = st->next; // la fin du premier nombre pointe sur le reste de la liste
                st->next = fh;*/
                if (fh == list->head){
                    list->head = sh; // le second nombre se positionne en tete de liste
                }
                else{
                    start->next = sh;
                }
                // on permute pour remettre les pointeurs dans l'ordre
                ox = fh;
                fh = sh;
                sh = ox;
                ox = ft;
                ft = st;
                st = ox;
                display(list);
            }
            start = ft->next;
            fh = start->next;
            ft = fh;
            while(ft->next->data != NULL){
                ft = ft->next;
            }
            n1[0] = '\0';
            n2[0] = '\0';
        } while (ft->next != interval && ft->next != list->tail && ft->next->next != interval);

        interval = sh;
        start = list->head;
        fh = start;
        ft = fh;
        while(ft->next->data != NULL){
            ft = fh->next;
        }
        if (ft->next->next == sh){ // le pointeur interval est sur le deuxième nombre donc on a trié tout le reste après
            interval = list->head;
        }
    }
    free(n1);
    free(n2);

    return 0;
}

void display(List* list){
    if(!list || !list->head){
        printf("Nothing to display...\n");
        return;
    }

    int c;
    c = lengthList(list);

    Element *p = list->head;
    printf("| ");
    for (p = list->head; p != NULL; p = p->next){
        printf(" %s | ", p->data);
    }
    printf("\n\n");

}

void destruct(List** list){
    if (!*list){
        printf("List is already destroyed...\n");
        return;
    }

    while((*list)->head->next != NULL){/*cetait pas la bonne condition*/
        Element* e = (*list)->head;
        printf("pointers bf : list head : %p, e : %p\n", (*list)->head, e);
        if((*list)->head != (*list)->tail){
            printf("if\n");
            (*list)->head = (*list)->head->next;
        }
        printf("free\n");
        printf("pointers af : list head : %p, e : %p\n", (*list)->head, e);
        free(e->data);
        free(e);
    }

    free(*list);

}

void display_menu(){
    printf("******MAIN MENU***********************\n");
    printf("**Choose an action by its key*********\n");
    printf("**************************************\n\n");
    printf("1 - Add a number at the beg. of the list\n");
    printf("2 - Add a number at the end of the list\n");
    printf("3 - Add a number at ...\n");
    printf("4 - Delete a number from...\n");
    printf("5 - Sort the list by ascending order\n");
    printf("6 - Display the list\n");
    printf("7 - Destroy the list\n");
    printf("8 - Quit\n");
    printf("**************************************\n\n");

}

int lengthList (List *list){

    if(!list){
        printf("list doesn't exist");
        return -1;
    }
    Element *parcour = list->head;
    int compteur = 0;

    while(parcour != list->tail){
        parcour = parcour->next;
        if(parcour->data == NULL)
            compteur++;
    }

    return compteur;
}

