#include <stdio.h>
#include <stdlib.h>
#include "tp3.h"

int main()
{
    if (DEBUG) printf("Initialization of ChainedLists...\n\n");

    int action = 0; // represents the choice of the user
    char str_number[100];
    int p = 0;
    //char test[5];

    printf("*****CHAINED LISTS*****\n");
    printf("**UTC - November 2017**\n");
    printf("*****W.Sha - Y.Ly******\n\n");

    if (DEBUG) printf("Initialization of an instance of List...\n\n");
    List* default_list;
    initialize(&default_list);

    while(action != 8){

        action = 0;
        // action prompt
        display_menu();
        printf("Your choice : ");
        fflush(stdin);
        scanf("%d", &action);

        while(action == 0 || action < 1 || action > 8){ // not a number or number not in interval
            display_menu();
            printf("Wrong input. Please retry [1-8] : ");
            fflush(stdin); // flushes the buffer
            scanf("%d", &action);
        }

        fflush(stdin);

        switch(action){
        case TOKEN_ADD_NB_BEG:
            printf("What do you want to add at the beginning ? ");
            scanf("%s", str_number);
            insert_beginning_list(&default_list, str_number);
            break;

        case TOKEN_ADD_NB_END:
            printf("What do you want to add at the end ? ");
            scanf("%s", str_number);
            insert_end_list(&default_list, str_number);
            break;

        case TOKEN_ADD_NB_SPC:
            printf("Which element do you want to add? ");
            scanf("%s", str_number);
            printf("\nWhich position do you want to add it at? ");
            scanf("%d", &p);
            insert_after_position(&default_list, str_number, p);
            break;

        case TOKEN_DEL_NB_SPC:
            printf("Which element do you want to remove? (please write the position)\n");
            scanf("%d", &p);
            remove_element(default_list, p);
            break;

        case TOKEN_SRT_LI_ASC:
            sort(default_list);
            break;

        case TOKEN_DSP_LI_STD:
            display(default_list);
            break;

        case TOKEN_RMX_LI_ALL:
            destruct(&default_list);
            initialize(&default_list);
            break;

        }

    }

    if (DEBUG) printf("ChainedLists will close...\n");
    return 0;
}
