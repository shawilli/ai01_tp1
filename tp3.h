#define N 5
#define DEBUG 1
#define TOKEN_ADD_NB_BEG 1
#define TOKEN_ADD_NB_END 2
#define TOKEN_ADD_NB_SPC 3
#define TOKEN_DEL_NB_SPC 4
#define TOKEN_SRT_LI_ASC 5
#define TOKEN_DSP_LI_STD 6
#define TOKEN_RMX_LI_ALL 7
#define TOKEN_PGM_GO_OUT 8

typedef struct element{
    char* data;
    struct element* next;
} Element;

typedef struct list{
    Element* head;
    Element* tail;
} List;

void initialize(List** list);
void insert_empty_list(List** list, char* str);
void insert_beginning_list(List** list, char* str);
void insert_end_list(List** list, char* str);
int insert_after_position(List** list, char* str, int p);
int remove_element(List* list, int p);
int compare(char* str1, char* str2);
int sort(List* list);
void display(List* list);
void destruct(List** list);

void display_menu();
int lengthList (List *list);
